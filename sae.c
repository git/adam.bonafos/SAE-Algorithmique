#include <stdio.h>
#include "sae.h"

int menu (void) {
  int rep ;
  printf("\n\t+-----------------------------------------------------+\n") ;
  printf("\t|  1) Créer un adhérent                               |\n") ;
  printf("\t|  2) Alimenter une carte                             |\n") ;
  printf("\t|  3) Désactiver/Activer une carte                    |\n") ;
  printf("\t|  4) Supprimer un adhérent                           |\n") ;
  printf("\t|  5) Afficher les infos d'un adhérent                |\n") ;
  printf("\t|  6) Afficher tous les adhérents                     |\n") ;
  printf("\t|  7) Afficher le nombre d'entrées par activités      |\n") ;
  printf("\t|     dans la journée                                 |\n") ;
  printf("\t|  8) Selectionner une activité                       |\n") ;
  printf("\t+-----------------------------------------------------+\n") ;
  printf("\t|              POUR QUITTER PRESSER : 0               |\n") ;
  printf("\t+-----------------------------------------------------+\n") ;
  
  printf("\n\nNuméro de l'action choisie : ") ;
  scanf("%d", &rep) ;

  printf("########################################\n\n") ;
  
  return rep ;
}


void globale (void) { 
  int choix = 10 , nbreAdh, numAdh, rep ;
  int soldes [100], dejaVenu[100]={0}, statutCarte[100] , numCompte[100],  tabActi[5]={0} ;
  printf("  ######### SYSTEME DÉMARRÉ #########\n\n") ;
  printf("\t     +-------------------------------------------+\n") ;
  printf("\t     |                ALTÈRE PUB                 |\n") ;
  printf("\t     +-------------------------------------------+\n\n") ;
  nbreAdh = chargement (soldes, statutCarte, numCompte) ;
  if (nbreAdh < 0) {
    printf("ERROR : FICHIER MAL CHARGÉ\n") ;
    return ;
  }
  
  choix = menu () ;
  while (choix != 0) {
    if (choix == 1) {
      nbreAdh = creerAdh (nbreAdh, numCompte, soldes, statutCarte) ;
    }
    else if (choix == 2) {
      printf("Numéro d'adhérents : ") ;
      scanf("%d", &numAdh) ;
      alimenterCarte (numAdh, soldes, nbreAdh) ;//manque vérife si adhérent dans la liste
    }
    else if (choix == 3) {
      printf("Numéro d'adhérents : ") ;
      scanf("%d", &numAdh) ;
      actDesactCarte (numAdh, statutCarte, numCompte, nbreAdh);
    }
    else if (choix == 4) {
      printf("Numéro d'adhérents : ") ;
      scanf("%d", &numAdh) ;
      suppAdh(numAdh, soldes, dejaVenu, numCompte,\
	      statutCarte, &nbreAdh) ;
    }
    else if (choix == 5) {
      printf("Numéro d'adhérents : ") ;
      scanf("%d", &numAdh) ;
      affichAdh (numAdh, soldes, dejaVenu, statutCarte,  numCompte, nbreAdh) ;
    }
    else if (choix == 6) {
      affichTousAdh (numAdh, soldes, dejaVenu, statutCarte,\
		     numCompte, nbreAdh) ;
    }
    else if (choix == 7) {
      affichActi(tabActi) ;
    }
    else if (choix == 8) {
      rep = menuActivite() ;
      activite (rep, soldes, dejaVenu, numCompte, statutCarte, nbreAdh, tabActi) ;
    }
    choix = menu () ;
  }
  sauvegarde (numCompte, soldes, statutCarte, nbreAdh) ;
}

int recherche (int numAdh, int tabNumAdh [], int nbreAdh) {
  int i ;
  for (i = 0 ; i < nbreAdh ; i ++) {
    if (tabNumAdh[i] == numAdh) return i ;
  }
  return -1 ;
}

int numDispo (int tabNumAdh [],int nbreAdh) {
  int i , research ;
  for (i = 0 ; i < 100 ; i++) {
    research = recherche(i, tabNumAdh, nbreAdh) ;
    if (research == -1) return i ;
  }
  return -1 ;
}

int creerAdh (int nbreAdh, int tabNumAdh [], int soldes [], int statutCarte []) {
  int numAdh = numDispo(tabNumAdh, nbreAdh) ;
  if (numAdh == -1) {
    printf("Erreur trop d'adhérents !\n") ;
    return 0;
  }
  printf("Numéro d'adhérent : %d\n", numAdh) ;
  tabNumAdh[nbreAdh] = numAdh ;
  soldes[nbreAdh] = 0 ;
  while (soldes[nbreAdh] == 0) { 
    alimenterCarte(nbreAdh, soldes, nbreAdh) ;
  }
  statutCarte[nbreAdh] = 1 ; // 0: désactivé / 1: activé
  nbreAdh ++ ;
  return nbreAdh ;
}


  
int chargement (int soldes [], int statutCarte [], int tabNumCompt []) {
  FILE * loadFile ;
  int solde, nbreAdh = 0, numCompte, estActive ;
  printf("chargement des données ...\n") ;
  loadFile = fopen("adherents.don", "r") ;
  if (loadFile == NULL) return -1;
  fscanf(loadFile, "%d %d %d\n", &numCompte, &solde, &estActive ) ;
  while (feof(loadFile) == 0) {
    soldes[nbreAdh] = solde ;
    statutCarte[nbreAdh] = estActive ;
    tabNumCompt[nbreAdh] = numCompte ;
    nbreAdh ++ ;
    fscanf(loadFile, "%d %d %d\n", &numCompte,\
	   &solde, &estActive ) ;
  }
  fscanf(loadFile, "%d %d %d\n", &numCompte,	\
	 &solde, &estActive ) ;
  soldes[nbreAdh] = solde ;
  statutCarte[nbreAdh] = estActive ;
  tabNumCompt[nbreAdh] = numCompte ;
  if (nbreAdh != 0) nbreAdh ++ ;
  fclose(loadFile) ;
  printf("données chargés\n") ;
  return nbreAdh ;
}

void alimenterCarte (int numAdh, int soldes[], int nbreAdh) {
  printf("  ######## ALIMENTER LA CARTE ########\n\n") ;
  int nbPointAchat ;
  float prix ;
  char valide ;
  
  printf("Vous avez : %d points\n", soldes[numAdh]) ;
  printf("Combien de points voulez-vous en\
 rajoutez (5 points = 5€) : ") ;
  scanf("%d%*c", &nbPointAchat) ;
  prix = nbPointAchat ;
  
  printf("Pour %d points vous devez payer : %.2f€\n",\
	 nbPointAchat, prix) ;
  printf("Valider l'achat (O:oui / N:non) : ") ;
  scanf("%c*c", &valide) ;

  if (valide == 'O') {
    printf ("\nPaiement en cours ... ") ;
    printf ("Paiement finalisé !\n") ;
    soldes[numAdh] += prix  ;
    if (prix > 50) {
      printf("Cadeau : 10 points offerts comme vous\
 avez crédité plus de 50€") ;
      soldes[numAdh] += 10 ;
    }
    printf("\nNouveau solde : %d points\n", soldes[numAdh]) ;
  }
  else printf("Rentrez un numéro d'adhérent valide\n") ;
}
   
void sauvegarde (int numCompte [], int soldes [], int statutCarte [],\
		 int nbreAdh) {
  FILE* save ;
  int i ;
  printf("sauvegarde ...\n") ;
  save = fopen("adherents.don", "w") ;
  for (i=0 ; i < nbreAdh ; i++) {
    fprintf(save,"%d %d %d\n", numCompte[i], soldes[i], statutCarte[i]) ;
  }

  fclose(save) ;
  printf("sauvegarde effectuée\n") ;
}

int menuActivite (void) {
  int rep ;
  printf("\n\t+-------------------------------------------------------+\n") ;
  printf("\t|  1) Entrée salle de sport                   6 points  |\n") ;
  printf("\t|  2) Entrée + coach                         25 points  |\n") ;
  printf("\t|  3) Entrée + boisson soft                   8 points  |\n") ;
  printf("\t|  4) Pinte de bière                          5 points  |\n") ;
  printf("\t|  5) Demi de bière                           3 points  |\n") ;
  printf("\t|  6) Cocktail                                8 points  |\n") ;
  /*printf("\t|  7) Tous les Jeudi :                       10 points  |\n") ;
  printf("\t|       A gagner, une girafe de bière + une             |\n") ;
  printf("\t|       entrée gratuite à la salle de sport             |\n") ;*/
  printf("\t+-------------------------------------------------------+\n") ;
  printf("\t|               POUR QUITTER PRESSER : 0                |\n") ;
  printf("\t+-------------------------------------------------------+\n") ;
  
  printf("\n\nNuméro de l'activité choisie : ") ;
  scanf("%d", &rep) ;

  printf("########################################\n\n") ;
  
  return rep ;
}

int depenserPts (int numAdh, int soldes [], int montantDebit, int tabActi [], int acti) {
  if (soldes[numAdh] > montantDebit) {
    soldes[numAdh] -= montantDebit ;
    printf("nouveau solde : %d\n", soldes[numAdh]) ;
    tabActi[acti] ++ ;
    return 1 ;
  }
  else {
    printf("Erreur : vous n'avez que %d point(s)\n", soldes[numAdh]) ;
    return 0 ;
  }
}
  
void activite (int choix, int soldes [], int dejaVenu [], int tabNumAdh [],\
	       int statutCarte [], int nbreAdh, int tabActi []) {
  int numAdh ;
  if (choix == 0) return ;
  printf("Numéro d'adhérent : ") ;
  scanf("%d", &numAdh) ;
  numAdh = recherche (numAdh, tabNumAdh, nbreAdh) ;
  if (numAdh != -1 && dejaVenu[numAdh] == 0) {
    if (statutCarte[numAdh] == 1) {
      
      if (choix == 1) {
	printf("Activité : Entrée salle de sport\n") ;
	dejaVenu[numAdh] = depenserPts(numAdh, soldes, 6, tabActi, 0) ;
      }
      else if (choix == 2) {
	printf("Activité : Entrée salle de sport + coach\n") ;
	dejaVenu[numAdh] = depenserPts(numAdh, soldes, 25, tabActi, 1) ;
      }
      else if (choix == 3) {
	printf("Activité : Entrée salle de sport + boisson soft\n") ;
	dejaVenu[numAdh] = depenserPts(numAdh, soldes, 8, tabActi, 2) ;
      }
      else if (choix == 4) {
	printf("Activité : Dégustation d'une pinte de bière\n") ;
	depenserPts(numAdh, soldes, 5, tabActi, 3) ;
      }
      else if (choix == 5) {
	printf("Activité : Dégustation d'un demi de bière\n") ;
	depenserPts(numAdh, soldes, 3, tabActi, 4) ;
      }
      else if (choix == 6) {
	printf("Activité : Dégustation d'un cocktail\n") ;
	depenserPts(numAdh, soldes, 8, tabActi, 5) ;
      }
      /*else if (choix == 7) {
	printf("Activité : Participation au concour\n") ;
	depenserPts(numAdh, soldes, 10) ;
      
	//la sauvegarde de paticipation n'est pas faite
	}*/
    }
    else {
      printf("Réactivé votre carte !") ;
    }
  }
  else {
    printf("Connectez-vous avec un numéro d'adhérent valide ou vous êtes déjà venu\n") ;
  }
}

void affichAdh (int numAdh, int soldes [], int dejaVenu[],\
		int statutCarte[] , int numCompte[], int nbreAdh) {
  numAdh = recherche (numAdh, numCompte, nbreAdh) ;
  if (numAdh != -1) {
    printf(" | Num %2d |", numCompte[numAdh]) ;
    printf("%5d points |", soldes[numAdh]) ;
    if (statutCarte[numAdh] == 1) printf(" Carte Activé    |") ;
    else printf(" Carte Désactivé |") ;
    if (dejaVenu[numAdh] == 1) printf(" Nombre de visite : 1") ;
    else printf(" Nombre de visite : 0") ;
    printf(" |");
  }
  else printf("Erreur : cette adhérent n'existe pas") ;
}

void affichTousAdh (int numAdh, int soldes [], int dejaVenu[],\
		int statutCarte[] , int numCompte[], int nbreAdh) {
  int i ;
  printf(" +---------------------------------------------------------------+\n") ;
  for (i = 0 ; i < nbreAdh ; i++) {
    printf(" | Num %2d |", numCompte[i]) ;
    printf("%5d points |", soldes[i]) ;
    if (statutCarte[i] == 1) printf(" Carte Activé    |") ;
    else printf(" Carte Désactivé |") ;
    if (dejaVenu[i] == 1) printf(" Nombre de visite : 1") ;
    else printf(" Nombre de visite : 0") ;
    printf(" |");
    /*affichAdh (i, soldes, dejaVenu,			\
      statutCarte, numCompte, nbreAdh) ;*/
    printf("\n");
  }
  printf(" +---------------------------------------------------------------+\n") ;
}

void actDesactCarte (int numAdh, int statutCarte [], int numCompte [], int nbreAdh) {
  int statut ;
  numAdh = recherche (numAdh, numCompte, nbreAdh) ;
  if (numAdh != -1) {
    printf("\t+---------------------------+\n") ;
    printf("\t|  0 : Désactiver la carte  |\n") ;
    printf("\t|  1 : Activer la carte     |\n") ;
    printf("\t+---------------------------+\n") ;
    printf("\nStatut : ") ;
    scanf("%d", &statut) ;
    if (statut == 1) {
      printf("Carte Activée\n") ;
      statutCarte[numAdh] = 1 ;
    }
    else if (statut == 0) {
      printf("Carte désactivée\n") ;
      statutCarte[numAdh] = 0 ;
    }
    else {
      printf("Erreur : rentrer 0 ou 1\n") ;
    }
  }
  else printf("Entrez un numéro d'adhérent valide\n") ;
}

void suppAdh(int numAdhSup, int soldes [], int dejaVenu [], int tabNumAdh [],\
	       int statutCarte [], int* nbreAdh) {
  int i ;
  numAdhSup = recherche (numAdhSup, tabNumAdh, *nbreAdh) ;
  if (numAdhSup != -1) {
    printf("L'adhérent %d va être supprimé\n", numAdhSup) ;
    for (i = numAdhSup ; i < *nbreAdh ; i++) {
      soldes[i] = soldes[i + 1] ;
      dejaVenu[i] = dejaVenu[i + 1] ;
      tabNumAdh[i] = tabNumAdh[i + 1] ;
      statutCarte[i] = statutCarte[i + 1] ;
    }
    printf("L'adhérent a était supprimé avec succès\n") ;
    *nbreAdh = *nbreAdh - 1 ;
  }
  else printf("Entrez un numéro d'adhérent valide\n") ;
}

void affichActi(int tabActi []) {
  printf("Statistiques :\n") ;
  printf("Entrée salle de sport : %d\n", tabActi[0]) ;
  printf("Entrée + coach :        %d\n", tabActi[1]) ;
  printf("Entrée + boisson soft : %d\n", tabActi[2]) ;
  printf("Pinte de bière :        %d\n", tabActi[3]) ;
  printf("Demi de bière :         %d\n", tabActi[4]) ;
  printf("Cocktail :              %d\n", tabActi[5]) ;
}

