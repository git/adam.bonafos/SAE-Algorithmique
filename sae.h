/**
   \file sae.c
   \author Adam BONAFOS
   \date 10/11/2022
   \brief ce fichier sert à faire fonctionner le programme du bar salle de sport : "altère pub".
*/



/**
   \brief fonction qui gère l'affichage et la saisie dans le menu principal
   \return le choix d'action de l'utilisateur
*/
int menu (void) ;



/**
   \brief fonction globale qui agit comme un menu principal
 */
void globale (void) ;



/**
   \brief fonction qui permet de creer un adhérent
   \param nbreAdh nombre d'adhérents
   \param[in, out] tabNumAdh tableau contenant tous les numéros d'adhérents
   \param[in, out] soldes tableau contenant les points de tous les adhérents
   \param[in, out] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \return le nouveau nombre d'adhérents
 */
int creerAdh (int nbreAdh, int tabNumAdh [], int soldes [], int statutCarte [] ) ;



/**
   \brief fonction qui charge les données (soldes, statutCarte et N°Compte) dans des tableaux
   \param[out] soldes tableau contenant les points de tous les adhérents 
   \param[out] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[out] tabNumAdh tableau contenant tous les numéros d'adhérents
 */
int chargement (int soldes [], int statutCarte [], int numCompte [] ) ;



/**
   \brief fonction qui sert à alimenter en points les cartes des adhérents
   \param numAdh Numéro d'adhérent auquel il faut ajouter des points
   \param[in, out] soldes tableau contenant les points de tous les adhérents 
   \param[in] nbreAdh nombre d'adhérents
 */
void alimenterCarte (int numAdh, int soldes [],  int nbreAdh) ;



/**
   \brief fonction qui sauvegarde les données (soldes, statutCarte et N°Compte)
   \b dans le fichier adherents.don
   \param[in] numCompte tableau contenant tous les numéros d'adhérents
   \param[in] soldes tableau contenant les points de tous les adhérents 
   \param[in] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in] nbreAdh nombre d'adhérents
 */
void sauvegarde (int numCompte [], int soldes [], int statutCarte [],\
		 int nbreAdh) ;



/**
   \brief fonction qui gère l'affichage et la saisie dans le sous menu des activités
   \return le choix d'activité de l'utilisateur
 */
int menuActivite (void) ;



/**
   \brief fonction qui "démarre" une activité selon le choix de l'utilisateur
   \param[in] choix choix d'activité fait par l'utilisateur
   \param[in, out] soldes tableau contenant les points de tous les adhérents 
   \param[in, out] dejaVenu tableau qui gère si un utilisateur est déjà
   \b rentré dans la salle de sport aujourd'hui
   \param[in] tabNumAdh tableau contenant tous les numéros d'adhérents
   \param[in] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in] nbreAdh nombre d'adhérents
   \param[in, out] tabActi tableau qui compte le nombre d'occurences
   \b journalières de chaques activités
 */
void activite (int choix, int soldes [], int dejaVenu [],\
	       int tabNumAdh [], int statutCarte [], int nbreAdh, int tabActi []) ;



/**
   \brief fonction qui affiche les informatinos d'un adhérent
   \param[in] numAdh numéro d'adhérent à afficher
   \param[in] soldes tableau contenant les points de tous les adhérents 
   \param[in] dejaVenu tableau qui gère si un utilisateur est déjà
   \param[in] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in] numCompte tableau contenant tous les numéros d'adhérents
   \param[in] nbreAdh nombre d'adhérents
 */
void affichAdh (int numAdh, int soldes [], int dejaVenu[],\
		int statutCarte[] , int numCompte[], int nbreAdh) ;



/**
   \brief fonction qui affiche tous les adhérents
   \param[in] numAdh numéro d'adhérent à afficher
   \param[in] soldes tableau contenant les points de tous les adhérents 
   \param[in] dejaVenu tableau qui gère si un utilisateur est déjà
   \b rentré dans la salle de sport aujourd'hui
   \param[in] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in] numCompte tableau contenant tous les numéros d'adhérents
   \param[in] nbreAdh nombre d'adhérents
 */
void affichTousAdh (int numAdh, int soldes [], int dejaVenu[],\
		      int statutCarte[] , int numCompte[], int nbreAdh) ;



/**
   \brief fonction qui permet d'activer ou de désactiver la carte d'un adhérent
   \param numAdh numéro d'adhérent
   \param[in, out] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in] numCompte tableau contenant tous les numéros d'adhérents
   \param[in] nbreAdh nombre d'adhérents
 */
void actDesactCarte (int numAdh, int statutCarte [], int numCompte [], int nbreAdh) ;



/**
   \brief fonction qui permet de supprimer un adhérent à partir de son numéro d'adhérent
   \param[in] numAdhSup numéro de l'adhérent à supprimer
   \param[in, out] soldes tableau contenant les points de tous les adhérents 
   \param[in, out] dejaVenu tableau qui gère si un utilisateur est déjà
   \b rentré dans la salle de sport aujourd'hui
   \param[in, out] tabNumAdh tableau contenant tous les numéros d'adhérents
   \param[in, out] statutCarte tableau contenant le statut des cartes de chaques adhérents
   \param[in, out] nbreAdh pointeur servant à modifier le nombre d'adhérents
 */
void suppAdh(int numAdhSup, int soldes [], int dejaVenu [], int tabNumAdh [], \
	     int statutCarte [], int * nbreAdh) ;



/**
   \brief fonction qui retire des points à un adhérents lorsqu'il fait une activité
   \param[in] numAdh numéro de l'adhérent qui fait l'activité
   \param[in, out] soldes tableau contenant les points de tous les adhérents 
   \param[in] montantDebit somme à débiter du compte de l'adhérent (prix de l'activité)
   \param[in, out] tabActi tableau qui compte le nombre d'occurences
   \b journalières de chaques activités
   \param[in] acti numéro de l'activité faite par l'adhérent (case du tableau tabActi
   \b où est stocké le compteur d'occurence de cette activité)
 */
int depenserPts (int numAdh, int soldes [], int montantDebit,\
		 int tabActi [], int acti) ;



/**
   \brief fonction qui affiche les statistiques de fréquentations pour chaque activités
   \param[in] tabActi tableau qui compte le nombre d'occurences
   \b journalières de chaques activités 
 */
void affichActi(int tabActi []) ;
